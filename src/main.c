#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "noiseg.h"
#include "arithmetic.h"
#include "cosft.h"
#include "cosf7.h"
#include "cosf9.h"
#include "cosf4.h"
#include "cosf2.h"
#include "cosf1.h"

void compare_cosf(void) {
  const int blocksize = 64;
  float *b_phase = 0;
  float *b_phase2pi = 0;
  float *b_cosf = 0;
  float *b_cosfa = 0;
  float *b_tmpf = 0;
  int   *b_tmpi = 0;
#define ALLOC(p) posix_memalign((void **)(&(p)), 256, blocksize * sizeof(*(p)))
  ALLOC(b_phase);
  ALLOC(b_phase2pi);
  ALLOC(b_cosf);
  ALLOC(b_cosfa);
  ALLOC(b_tmpf);
  ALLOC(b_tmpi);
#undef ALLOC

  { // quality
#define ALGORITHMS 17
    const char *name[ALGORITHMS] =
      { "513 linear"
      , "1025 linear"
      , "2049 linear"
      , "4097 linear"
      , "8193 linear"
      , "512×2 linear"
      , "1024×2 linear"
      , "2048×2 linear"
      , "4096×2 linear"
      , "8192×2 linear"
      , "16×4 cubic"
      , "32×4 cubic"
      , "64×4 cubic"
      , "128×4 cubic"
      , "256×4 cubic"
      , "7 order poly"
      , "9 order poly"
      };
    float  abserr[ALGORITHMS];
    double rmserr[ALGORITHMS];
    for (int algorithm = 0; algorithm < ALGORITHMS; ++algorithm) {
      abserr[algorithm] = 0;
      rmserr[algorithm] = 0;
    }
    for (int i = 0; i < 1 << 24; ++i) {
      for (int j = 0; j < blocksize; ++j) {
        int k = i * blocksize + j;
        float p = k / ((double) (1 << 24) * blocksize);
        b_phase[j] = p;
      }
      mulffs(blocksize, 6.283185307179586f, b_phase, b_phase2pi);
      cosfs(blocksize, b_phase2pi, b_cosf);
      for (int algorithm = 0; algorithm < ALGORITHMS; ++algorithm) {
        switch (algorithm) {
          case 0:
          case 1:
          case 2:
          case 3:
          case 4:
            cosf1s2(blocksize, algorithm + 9, b_phase, b_cosfa, b_tmpf, b_tmpi);
            break;
          case 5:
          case 6:
          case 7:
          case 8:
          case 9:
            cosf2s2(blocksize, algorithm + 4, b_phase, b_cosfa, b_tmpf, b_tmpi);
            break;
          case 10:
          case 11:
          case 12:
          case 13:
          case 14:
            cosf4s2(blocksize, algorithm - 6, b_phase, b_cosfa, b_tmpf, b_tmpi);
            break;
          case 15:
            cosf7s2(blocksize, b_phase, b_cosfa, b_tmpf);
            break;
          case 16:
            cosf9s2(blocksize, b_phase, b_cosfa, b_tmpf);
            break;
        }
        abserr[algorithm] = maximumabssubfs(blocksize, abserr[algorithm], b_cosfa, b_cosf);
        rmserr[algorithm] += sumsqrsubfs(blocksize, b_cosfa, b_cosf);
      }
    }
    for (int algorithm = 0; algorithm < ALGORITHMS; ++algorithm) {
      rmserr[algorithm] /= 1 << 24;
      rmserr[algorithm] /= blocksize;
      rmserr[algorithm] = sqrt(rmserr[algorithm]);
    }
    FILE *f = fopen("cosf_quality.txt", "wb");
    fprintf(f, "# algorithm\tabserr\trmserr\n");
    for (int algorithm = 0; algorithm < ALGORITHMS; ++algorithm) {
      fprintf(f, "\"%s\"\t%.16g\t%.16g\n", name[algorithm], abserr[algorithm], rmserr[algorithm]);
    }
    fclose(f);
#undef ALGORITHMS
  } // quality

  { // speed
#define MODES 4
#define ALGORITHMS 37
    const char *modes[MODES] =
      { "cosf_speed_seq_small.txt"
      , "cosf_speed_rnd_small.txt"
      , "cosf_speed_seq_large.txt"
      , "cosf_speed_rnd_large.txt"
      };
    const char *name[ALGORITHMS] =
      { "513 linear 1"
      , "1025 linear 1"
      , "2049 linear 1"
      , "4097 linear 1"
      , "8193 linear 1"
      , "513 linear 2"
      , "1025 linear 2"
      , "2049 linear 2"
      , "4097 linear 2"
      , "8193 linear 2"
      , "512×2 linear 1"
      , "1024×2 linear 1"
      , "2048×2 linear 1"
      , "4096×2 linear 1"
      , "8192×2 linear 1"
      , "512×2 linear 2"
      , "1024×2 linear 2"
      , "2048×2 linear 2"
      , "4096×2 linear 2"
      , "8192×2 linear 2"
      , "16×4 cubic 1"
      , "32×4 cubic 1"
      , "64×4 cubic 1"
      , "128×4 cubic 1"
      , "256×4 cubic 1"
      , "16×4 cubic 2"
      , "32×4 cubic 2"
      , "64×4 cubic 2"
      , "128×4 cubic 2"
      , "256×4 cubic 2"
      , "7 order poly 1"
      , "7 order poly 2"
      , "9 order poly 1"
      , "9 order poly 2"
      , "pd costab 1"
      , "pd costab 2"
      , "libm cosf()"
      };
    for (int mode = 0; mode < 4; ++mode) {
      FILE *f = fopen(modes[mode], "wb");
      fprintf(f, "# algorithm\tseconds\toverheads\n\n");
      double tnull = 0;
      for (int algorithm = -1; algorithm < ALGORITHMS; ++algorithm) {
        struct timespec then, now;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &then);
        uint32_t seed = 1;
        for (int i = 0; i < 1 << 24; ++i) {
          if (mode & 1) {
            seed = noisegifs(blocksize, seed, b_tmpf);
          } else {
            for (int j = 0; j < blocksize; ++j) {
              int k = ((i * blocksize + j) & ((1 << 16) - 1));
              b_tmpf[j] = k / (float) (1 << 16);
            }
          }
          if (mode & 2) {
            mulffs(blocksize, 5.0f, b_tmpf, b_phase);
          } else {
            mulffs(blocksize, 0.5f, b_tmpf, b_phase);
          }
          mulffs(blocksize, 6.283185307179586f, b_phase, b_phase2pi);
          switch (algorithm) {
            case -1:
              // overheads
              break;
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
              cosf1s1(blocksize, algorithm + 9, b_phase, b_cosfa);
              break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
              cosf1s2(blocksize, algorithm + 4, b_phase, b_cosfa, b_tmpf, b_tmpi);
              break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
              cosf2s1(blocksize, algorithm - 1, b_phase, b_cosfa);
              break;
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
              cosf2s2(blocksize, algorithm - 6, b_phase, b_cosfa, b_tmpf, b_tmpi);
              break;
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
              cosf4s1(blocksize, algorithm - 16, b_phase, b_cosfa);
              break;
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
              cosf4s2(blocksize, algorithm - 21, b_phase, b_cosfa, b_tmpf, b_tmpi);
              break;
            case 30:
              cosf7s1(blocksize, b_phase, b_cosfa);
              break;
            case 31:
              cosf7s2(blocksize, b_phase, b_cosfa, b_tmpf);
              break;
            case 32:
              cosf9s1(blocksize, b_phase, b_cosfa);
              break;
            case 33:
              cosf9s2(blocksize, b_phase, b_cosfa, b_tmpf);
              break;
            case 34:
              cosfts1(blocksize, b_phase2pi, b_cosfa);
              break;
            case 35:
              cosfts2(blocksize, b_phase2pi, b_cosfa);
              break;
            case 36:
              cosfs(blocksize, b_phase2pi, b_cosfa);
              break;
          }
        }
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &now);
        double tdelta = (now.tv_sec  - then.tv_sec )
                      + (now.tv_nsec - then.tv_nsec) / 1.0e9;
        if (algorithm < 0) {
          tnull = tdelta;
        } else {
          fprintf(f, "\"%s\"\t%.16g\t%.16g\n", name[algorithm], tdelta - tnull, tnull);
          fflush(f);
        }
      }
      fclose(f);
    }
#undef MODES
#undef ALGORITHMS
  } // speed

  { // waveforms
#define ALGORITHMS 18
    const char *name[ALGORITHMS] =
      { "cosf1-9.raw"
      , "cosf1-10.raw"
      , "cosf1-11.raw"
      , "cosf1-12.raw"
      , "cosf1-13.raw"
      , "cosf2-9.raw"
      , "cosf2-10.raw"
      , "cosf2-11.raw"
      , "cosf2-12.raw"
      , "cosf2-13.raw"
      , "cosf4-4.raw"
      , "cosf4-5.raw"
      , "cosf4-6.raw"
      , "cosf4-7.raw"
      , "cosf4-8.raw"
      , "cosf7.raw"
      , "cosf9.raw"
      , "cosft.raw"
      };
    FILE *fs[18];
    for (int algorithm = 0; algorithm < ALGORITHMS; ++algorithm) {
      fs[algorithm] = fopen(name[algorithm], "wb");
    }
    double phase = 0.75;
    for (int i = 0; i < (5 * 48000) / blocksize; ++i) {
      for (int j = 0; j < blocksize; ++j) {
        b_phase[j] = phase;
        int k = i * blocksize + j;
        double pitch = 20000 * pow(0.5, k / 24000.0);
        double increment = pitch / 48000.0;
        phase += increment;
        int q = phase;
        phase -= q;
      }
      mulffs(blocksize, 6.283185307179586f, b_phase, b_phase2pi);
      cosfs(blocksize, b_phase2pi, b_cosf);
      for (int algorithm = 0; algorithm < ALGORITHMS; ++algorithm) {
        switch (algorithm) {
          case 0:
          case 1:
          case 2:
          case 3:
          case 4:
            cosf1s2(blocksize, algorithm + 9, b_phase, b_cosfa, b_tmpf, b_tmpi);
            break;
          case 5:
          case 6:
          case 7:
          case 8:
          case 9:
            cosf2s2(blocksize, algorithm + 4, b_phase, b_cosfa, b_tmpf, b_tmpi);
            break;
          case 10:
          case 11:
          case 12:
          case 13:
          case 14:
            cosf4s2(blocksize, algorithm - 6, b_phase, b_cosfa, b_tmpf, b_tmpi);
            break;
          case 15:
            cosf7s2(blocksize, b_phase, b_cosfa, b_tmpf);
            break;
          case 16:
            cosf9s2(blocksize, b_phase, b_cosfa, b_tmpf);
            break;
          case 17:
            cosfts2(blocksize, b_phase, b_cosfa);
            break;
        }
        subfs(blocksize, b_cosfa, b_cosf, b_tmpf);
        fwrite(b_tmpf, sizeof(*b_tmpf) * blocksize, 1, fs[algorithm]);
      }
    }
    for (int algorithm = 0; algorithm < ALGORITHMS; ++algorithm) {
      fclose(fs[algorithm]);
    }
#undef ALGORITHMS
  }

  free(b_phase);
  free(b_phase2pi);
  free(b_cosf);
  free(b_cosfa);
  free(b_tmpf);
  free(b_tmpi);
}

int main() {
  cosf1_initialize();
  cosf2_initialize();
  cosf4_initialize();
  cosft_initialize();
  compare_cosf();
  return 0;
}
