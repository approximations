#include "cosf9.h"

#include <math.h>

#define   likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

// postcondition: 0 <= phase <= 1
static inline float cosf9_reduce(float phase) {
  float p = fabsf(phase);
  // p >= 0
  if (likely(p < (1 << 24))) {
    int q = p;
    return p - q;
  } else {
    if (unlikely(isnanf(p) || isinff(p))) {
      // return NaN
      return p - p;
    } else {
      // int could overflow, and it will be integral anyway
      return 0.0f;
    }
  }
}

// precondition: 0 <= phase <= 1
static inline float cosf9_unsafe(float phase) {
  float p = fabsf(4.0f * phase - 2.0f) - 1.0f;
  // p in -1 .. 1
  float s
    = 1.5707963267948965580e+00f * p
    - 6.4596271553942852250e-01f * p * p * p
    + 7.9685048314861006702e-02f * p * p * p * p * p
    - 4.6672571910271187789e-03f * p * p * p * p * p * p * p
    + 1.4859762069630022552e-04f * p * p * p * p * p * p * p * p * p;
  // compiler figures out optimal simd multiplications
  return s;
}

static inline float cosf9(float phase) {
  return cosf9_unsafe(cosf9_reduce(phase));
}

extern void cosf9s1(int n, const float * restrict in, float * restrict out) {
  while (n--) {
    *out++ = cosf9(*in++);
  }
}

extern void cosf9s2(int n, const float * restrict in, float * restrict out, float * restrict tmp) {
  float * restrict tmp2 = tmp;
  int m = n;
  while (n--) {
    *tmp++ = cosf9_reduce(*in++);
  }
  while (m--) {
    *out++ = cosf9_unsafe(*tmp2++);
  }
}
