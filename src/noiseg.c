#include "noiseg.h"

// seed 1 has period 2^30
static inline uint32_t noiseg_u32(uint32_t seed) {
  return 1640531525u * seed;
}

static inline float i32_to_f32(int32_t i) {
  return i / 2147483648.f;
}

static inline float u32_to_f32(uint32_t u) {
  return u / 4294967296.f;
}

extern uint32_t noisegufs(int n, uint32_t seed, float * restrict out) {
  while (n--) {
    *out++ = u32_to_f32(seed = noiseg_u32(seed));
  }
  return seed;
}

extern uint32_t noisegifs(int n, uint32_t seed, float * restrict out) {
  while (n--) {
    *out++ = i32_to_f32(seed = noiseg_u32(seed));
  }
  return seed;
}
