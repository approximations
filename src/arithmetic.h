#ifndef ARITHMETIC_H
#define ARITHMETIC_H 1

extern void addfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out);
extern void subfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out);
extern void mulfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out);
extern void mulffs(int n, float in1, const float * restrict in2, float * restrict out);
extern void divfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out);
extern void absfs(int n, const float * restrict in, float * restrict out);
extern void cosfs(int n, const float * restrict in, float * restrict out);
extern float minimumfs(int n, float minimum, const float * restrict in);
extern float maximumfs(int n, float maximum, const float * restrict in);
extern float maximumabssubfs(int n, float maximum, const float * restrict in1, const float * restrict in2);
extern float sumsqrsubfs(int n, const float * restrict in1, const float * restrict in2);

#endif
