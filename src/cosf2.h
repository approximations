#ifndef COSF2_H
#define COSF2_H 1

extern void cosf2_initialize(void);
extern void cosf2s1(int n, int index, const float * restrict in, float * restrict out);
extern void cosf2s2(int n, int index, const float * restrict in, float * restrict out, float * restrict tmp_phase, int * restrict tmp_index);

#endif
