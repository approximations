#include "cosf4.h"

#include <math.h>

#define COSTABCOUNT 9

static float cosf4_table[COSTABCOUNT][1 << (COSTABCOUNT - 1)][4];

extern void cosf4_initialize(void) {
  for (int i = 0; i < COSTABCOUNT; ++i) {
    int size = 1 << i;
    double s = 6.283185307179586 / size;
    for (int k = 0; k < size; ++k) {
      double x0 = s *  k;
      double x1 = s * (k + 1);
      double p0 =      cos(x0);
      double m0 = -s * sin(x0);
      double p1 =      cos(x1);
      double m1 = -s * sin(x1);
      double c0 = p0;
      double c1 = m0;
      double c2 = - 3 * p0 - 2 * m0 + 3 * p1 - m1;
      double c3 =   2 * p0 +     m0 - 2 * p1 + m1;
      cosf4_table[i][k][0] = c0;
      cosf4_table[i][k][1] = c1;
      cosf4_table[i][k][2] = c2;
      cosf4_table[i][k][3] = c3;
    }
  }
}

static inline float cosf4(float phase, int index, float size, int mask) {
  float p = fabsf(phase) * size;
  int q = p;
  p -= q;
  int i = q & mask;
  float x1 = p;
  float x2 = p * p;
  float x3 = p * p * p;
  float c0 = cosf4_table[index][i][0];
  float c1 = cosf4_table[index][i][1];
  float c2 = cosf4_table[index][i][2];
  float c3 = cosf4_table[index][i][3];
  return c0 + x1 * c1 + x2 * c2 + x3 * c3;
}

static inline void cosf4_reduce(float phase, float size, int mask, float * restrict out_phase, int * restrict out_index) {
  float p = fabsf(phase) * size;
  int q = p;
  p -= q;
  int i = q & mask;
  *out_phase = p;
  *out_index = i;
}

static inline float cosf4_unsafe(float p, int i, int index) {
  const float * cs = &cosf4_table[index][i][0];
  float x1 = p;
  float x2 = p * p;
  float x3 = p * p * p;
  float c0 = cs[0];
  float c1 = cs[1];
  float c2 = cs[2];
  float c3 = cs[3];
  return c0 + x1 * c1 + x2 * c2 + x3 * c3;
}

extern void cosf4s1(int n, int index, const float * restrict in, float * restrict out) {
  float size = 1 << index;
  int mask = (1 << index) - 1;
  while (n--) {
    *out++ = cosf4(*in++, index, size, mask);
  }
}

extern void cosf4s2(int n, int index, const float * restrict in, float * restrict out, float * restrict tmp_phase, int * restrict tmp_index) {
  float size = 1 << index;
  int mask = (1 << index) - 1;
  float * restrict tmp2_phase = tmp_phase;
  int * restrict tmp2_index = tmp_index;
  int m = n;
  while (n--) {
    cosf4_reduce(*in++, size, mask, tmp_phase++, tmp_index++);
  }
  while (m--) {
    *out++ = cosf4_unsafe(*tmp2_phase++, *tmp2_index++, index);
  }
}
