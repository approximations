#include <math.h>

extern void addfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out) {
  while (n--) {
    *out++ = *in1++ + *in2++;
  }
}

extern void subfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out) {
  while (n--) {
    *out++ = *in1++ - *in2++;
  }
}

extern void mulfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out) {
  while (n--) {
    *out++ = *in1++ * *in2++;
  }
}

extern void mulffs(int n, float in1, const float * restrict in2, float * restrict out) {
  while (n--) {
    *out++ = in1 * *in2++;
  }
}


extern void divfs(int n, const float * restrict in1, const float * restrict in2, float * restrict out) {
  while (n--) {
    *out++ = *in1++ / *in2++;
  }
}

extern void absfs(int n, const float * restrict in, float * restrict out) {
  while (n--) {
    *out++ = fabsf(*in++);
  }
}

extern void cosfs(int n, const float * restrict in, float * restrict out) {
  while (n--) {
    *out++ = cosf(*in++);
  }
}

extern float minimumfs(int n, float minimum, const float * restrict in) {
  while (n--) {
    minimum = fminf(minimum, *in++);
  }
  return minimum;
}

extern float maximumfs(int n, float maximum, const float * restrict in) {
  while (n--) {
    maximum = fmaxf(maximum, *in++);
  }
  return maximum;
}

extern float maximumabssubfs(int n, float maximum, const float * restrict in1, const float * restrict in2) {
  while (n--) {
    maximum = fmaxf(maximum, fabsf(*in1++ - *in2++));
  }
  return maximum;
}

extern float sumsqrsubfs(int n, const float * restrict in1, const float * restrict in2) {
  float sum = 0;
  while (n--) {
    float d = *in1++ - *in2++;
    sum += d * d;
  }
  return sum;
}
