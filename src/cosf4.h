#ifndef COSF4_H
#define COSF4_H 1

extern void cosf4_initialize(void);
extern void cosf4s1(int n, int index, const float * restrict in, float * restrict out);
extern void cosf4s2(int n, int index, const float * restrict in, float * restrict out, float * restrict tmp_phase, int * restrict tmp_index);

#endif
