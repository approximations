output_precision(20);
inverse([        1,          1,          1,          1
        ;        3,          5,          7,          9
        ;        6,         20,         42,         72
        ; (2/pi)^3,   (2/pi)^5,   (2/pi)^7,   (2/pi)^9 ]
 ) *    [ 1 - pi/2,      -pi/2,    -pi^2/4, sin(1) - 1 ]'
