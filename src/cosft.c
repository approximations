#include "cosft.h"

/* Copyright (c) 1997-1999 Miller Puckette.
* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.  */

#include <math.h>
#include <stdint.h>

#define COSTABSIZE 512
#define UNITBIT32 1572864.  /* 3*2^19; bit 32 has place value 1 */
#define HIOFFSET 1
#define LOWOFFSET 0

union tabfudge
{
    double tf_d;
    int32_t tf_i[2];
};

/* ------------------------ cos~ ----------------------------- */

static float cos_table[COSTABSIZE + 1];

static inline float cospd1(float in)
{
    float *tab = cos_table, *addr, f1, f2, frac;
    double dphase;
    int normhipart;
    union tabfudge tf;
    
    tf.tf_d = UNITBIT32;
    normhipart = tf.tf_i[HIOFFSET];

        dphase = (double)(in * (float)(COSTABSIZE)) + UNITBIT32;
        tf.tf_d = dphase;
        addr = tab + (tf.tf_i[HIOFFSET] & (COSTABSIZE-1));
        tf.tf_i[HIOFFSET] = normhipart;
        frac = tf.tf_d - UNITBIT32;
        f1 = addr[0];
        f2 = addr[1];
        return f1 + frac * (f2 - f1);
}

static inline float cospd2(float in, int normhipart)
{
  float *tab = cos_table, *addr, f1, f2, frac;
  double dphase;
  union tabfudge tf;
  dphase = (double)(in * (float)(COSTABSIZE)) + UNITBIT32;
  tf.tf_d = dphase;
  addr = tab + (tf.tf_i[HIOFFSET] & (COSTABSIZE-1));
  tf.tf_i[HIOFFSET] = normhipart;
  frac = tf.tf_d - UNITBIT32;
  f1 = addr[0];
  f2 = addr[1];
  return f1 + frac * (f2 - f1);
}

extern void cosft_initialize(void)
{
    int i;
    float *fp, phase, phsinc = (2. * 3.14159) / COSTABSIZE;
    
    for (i = COSTABSIZE + 1, fp = cos_table, phase = 0; i--;
        fp++, phase += phsinc)
            *fp = cos(phase);
}

extern void cosfts1(int n, const float * restrict in, float * restrict out) {
  while (n--) {
    *out++ = cospd1(*in++);
  }
}

extern void cosfts2(int n, const float * restrict in, float * restrict out) {
  int normhipart;
  union tabfudge tf;
  tf.tf_d = UNITBIT32;
  normhipart = tf.tf_i[HIOFFSET];
  while (n--) {
    *out++ = cospd2(*in++, normhipart);
  }
}
