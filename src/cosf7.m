output_precision(20);
inverse([        1,          1,          1
        ;        3,          5,          7
        ;        6,         20,         42 ]
 ) *    [ 1 - pi/2,      -pi/2,    -pi^2/4 ]'
