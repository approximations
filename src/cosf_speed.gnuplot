set term pngcairo enhanced font "LMSans10" size 1872,702
set xlabel "Algorithm"
set ylabel "Time (seconds)"
set style line 1 lt 1 lc rgb "#555555"
set style line 2 lt 2 lc rgb "#aaaaaa"
set boxwidth 0.666
set style fill solid
set style histogram rowstacked
set style data histogram
set xtics in nomirror rotate by -30
set ytics in
set grid
set grid noxtics
unset key
set title "Comparison of cosine approximation speed (randomized small input)."
set output "cosf_speed_rnd_small.png"
plot [-0.666:36.666][0:] "./cosf_speed_rnd_small.txt" u 2:xtic(1) lc rgb "#555555", '' u 3 lc rgb "#aaaaaa"
# replot as first time isn't aligned well
set title "Comparison of cosine approximation speed (randomized small input)."
set output "cosf_speed_rnd_small.png"
plot [-0.666:36.666][0:] "./cosf_speed_rnd_small.txt" u 2:xtic(1) lc rgb "#555555", '' u 3 lc rgb "#aaaaaa"
set title "Comparison of cosine approximation speed (randomized large input)."
set output "cosf_speed_rnd_large.png"
plot [-0.666:36.666][0:] "./cosf_speed_rnd_large.txt" u 2:xtic(1) lc rgb "#555555", '' u 3 lc rgb "#aaaaaa"
set title "Comparison of cosine approximation speed (sequential small input)."
set output "cosf_speed_seq_small.png"
plot [-0.666:36.666][0:] "./cosf_speed_seq_small.txt" u 2:xtic(1) lc rgb "#555555", '' u 3 lc rgb "#aaaaaa"
set title "Comparison of cosine approximation speed (sequential large input)."
set output "cosf_speed_seq_large.png"
plot [-0.666:36.666][0:] "./cosf_speed_seq_large.txt" u 2:xtic(1) lc rgb "#555555", '' u 3 lc rgb "#aaaaaa"
