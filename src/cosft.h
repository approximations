#ifndef COSFT_H
#define COSFT_H 1

extern void cosft_initialize(void);
extern void cosfts1(int n, const float * restrict in, float * restrict out);
extern void cosfts2(int n, const float * restrict in, float * restrict out);

#endif
