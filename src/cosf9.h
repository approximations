#ifndef COSF9_H
#define COSF9_H 1

extern void cosf9s1(int n, const float * restrict in, float * restrict out);
extern void cosf9s2(int n, const float * restrict in, float * restrict out, float * restrict tmp);

#endif
