#include "cosf1.h"

#include <math.h>
#include <stdint.h>

#define COSTABCOUNT 14

static float cosf1_table[COSTABCOUNT][(1 << (COSTABCOUNT - 1)) + 1];

extern void cosf1_initialize(void) {
  for (int i = 0; i < COSTABCOUNT; ++i) {
    int size = 1 << i;
    double s = 6.283185307179586 / size;
    for (int k = 0; k <= size; ++k) {
      double x0 = s *  k;
      double p0 = cos(x0);
      cosf1_table[i][k] = p0;
    }
  }
}

static inline float cosf1(float phase, int index, float size, int mask) {
  float p = fabsf(phase) * size;
  int q = p;
  p -= q;
  int i = q & mask;
  float x1 = p;
  float c0 = cosf1_table[index][i];
  float c1 = cosf1_table[index][i + 1];
  return c0 + x1 * (c1 - c0);
}

static inline void cosf1_reduce(float phase, float size, int mask, float * restrict out_phase, int * restrict out_index) {
  float p = fabsf(phase) * size;
  int q = p;
  p -= q;
  int i = q & mask;
  *out_phase = p;
  *out_index = i;
}

static inline float cosf1_unsafe(float p, int i, int index) {
  const float * cs = &cosf1_table[index][i];
  float x1 = p;
  float c0 = cs[0];
  float c1 = cs[1];
  return c0 + x1 * (c1 - c0);
}

extern void cosf1s1(int n, int index, const float * restrict in, float * restrict out) {
  float size = 1 << index;
  int mask = (1 << index) - 1;
  while (n--) {
    *out++ = cosf1(*in++, index, size, mask);
  }
}

extern void cosf1s2(int n, int index, const float * restrict in, float * restrict out, float * restrict tmp_phase, int * restrict tmp_index) {
  float size = 1 << index;
  int mask = (1 << index) - 1;
  float * restrict tmp2_phase = tmp_phase;
  int * restrict tmp2_index = tmp_index;
  int m = n;
  while (n--) {
    cosf1_reduce(*in++, size, mask, tmp_phase++, tmp_index++);
  }
  while (m--) {
    *out++ = cosf1_unsafe(*tmp2_phase++, *tmp2_index++, index);
  }
}
