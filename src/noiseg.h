#ifndef NOISEG_H
#define NOISEG_H 1

#include <stdint.h>

extern uint32_t noisegufs(int n, uint32_t seed, float * restrict out);
extern uint32_t noisegifs(int n, uint32_t seed, float * restrict out);

#endif
