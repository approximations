#include "cosf2.h"

#include <math.h>
#include <stdint.h>

#define COSTABCOUNT 14

static float cosf2_table[COSTABCOUNT][1 << (COSTABCOUNT - 1)][2];

extern void cosf2_initialize(void) {
  for (int i = 0; i < COSTABCOUNT; ++i) {
    int size = 1 << i;
    double s = 6.283185307179586 / size;
    for (int k = 0; k < size; ++k) {
      double x0 = s *  k;
      double x1 = s * (k + 1);
      double p0 = cos(x0);
      double p1 = cos(x1);
      double m0 = p1 - p0;
      cosf2_table[i][k][0] = p0;
      cosf2_table[i][k][1] = m0;
    }
  }
}

static inline float cosf2(float phase, int index, float size, int mask) {
  float p = fabsf(phase) * size;
  int q = p;
  p -= q;
  int i = q & mask;
  float x1 = p;
  float c0 = cosf2_table[index][i][0];
  float c1 = cosf2_table[index][i][1];
  return c0 + x1 * c1;
}

static inline void cosf2_reduce(float phase, float size, int mask, float * restrict out_phase, int * restrict out_index) {
  float p = fabsf(phase) * size;
  int q = p;
  p -= q;
  int i = q & mask;
  *out_phase = p;
  *out_index = i;
}

static inline float cosf2_unsafe(float p, int i, int index) {
  const float * cs = &cosf2_table[index][i][0];
  float x1 = p;
  float c0 = cs[0];
  float c1 = cs[1];
  return c0 + x1 * c1;
}

extern void cosf2s1(int n, int index, const float * restrict in, float * restrict out) {
  float size = 1 << index;
  int mask = (1 << index) - 1;
  while (n--) {
    *out++ = cosf2(*in++, index, size, mask);
  }
}

extern void cosf2s2(int n, int index, const float * restrict in, float * restrict out, float * restrict tmp_phase, int * restrict tmp_index) {
  float size = 1 << index;
  int mask = (1 << index) - 1;
  float * restrict tmp2_phase = tmp_phase;
  int * restrict tmp2_index = tmp_index;
  int m = n;
  while (n--) {
    cosf2_reduce(*in++, size, mask, tmp_phase++, tmp_index++);
  }
  while (m--) {
    *out++ = cosf2_unsafe(*tmp2_phase++, *tmp2_index++, index);
  }
}
