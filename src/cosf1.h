#ifndef COSF1_H
#define COSF1_H 1

extern void cosf1_initialize(void);
extern void cosf1s1(int n, int index, const float * restrict in, float * restrict out);
extern void cosf1s2(int n, int index, const float * restrict in, float * restrict out, float * restrict tmp_phase, int * restrict tmp_index);

#endif
