#include "cosf7.h"

#include <math.h>

#define   likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

// postcondition: 0 <= phase <= 1
static inline float cosf7_reduce(float phase) {
  float p = fabsf(phase);
  // p >= 0
  if (likely(p < (1 << 24))) {
    int q = p;
    return p - q;
  } else {
    if (unlikely(isnanf(p) || isinff(p))) {
      // return NaN
      return p - p;
    } else {
      // int could overflow, and it will be integral anyway
      return 0.0f;
    }
  }
}

// precondition: 0 <= phase <= 1
static inline float cosf7_unsafe(float phase) {
  float p = fabsf(4.0f * phase - 2.0f) - 1.0f;
  // p in -1 .. 1
  float s
    = 1.5707963267948965580e+00f * p
    - 6.4581411791873211126e-01f * p * p * p
    + 7.9239255452774770561e-02f * p * p * p * p * p
    - 4.2214643289391062808e-03f * p * p * p * p * p * p * p;
  // compiler figures out optimal simd multiplications
  return s;
}

static inline float cosf7(float phase) {
  return cosf7_unsafe(cosf7_reduce(phase));
}


extern void cosf7s1(int n, const float * restrict in, float * restrict out) {
  while (n--) {
    *out++ = cosf7(*in++);
  }
}

extern void cosf7s2(int n, const float * restrict in, float * restrict out, float * restrict tmp) {
  float * restrict tmp2 = tmp;
  int m = n;
  while (n--) {
    *tmp++ = cosf7_reduce(*in++);
  }
  while (m--) {
    *out++ = cosf7_unsafe(*tmp2++);
  }
}
