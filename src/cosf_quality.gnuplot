set term pngcairo mono enhanced font "LMSans10" size 1872,702
set xlabel "Algorithm"
set ylabel "Error (-log_2 E)"
set boxwidth 0.333
set style fill solid
set xtics in nomirror offset first 0.2,0 rotate by -30
set ytics 1 in
set grid
set grid noxtics
unset key
set title "Comparison of cosine approximation quality."
set output "cosf_quality.png"
plot [-0.666:16.666][12:24] \
  "./cosf_quality.txt" u (column(0)-0.2):(-log(column(2))/log(2)):xtic(1) w boxes t "Maximum absolute error" lc rgb "#555555", \
  "./cosf_quality.txt" u (column(0)+0.2):(-log(column(3))/log(2))         w boxes t "Root mean square error" lc rgb "#aaaaaa"
# replot as first time isn't aligned well
set title "Comparison of cosine approximation quality."
set output "cosf_quality.png"
plot [-0.666:16.666][12:24] \
  "./cosf_quality.txt" u (column(0)-0.2):(-log(column(2))/log(2)):xtic(1) w boxes t "Maximum absolute error" lc rgb "#555555", \
  "./cosf_quality.txt" u (column(0)+0.2):(-log(column(3))/log(2))         w boxes t "Root mean square error" lc rgb "#aaaaaa"
