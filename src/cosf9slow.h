#ifndef COSF9SLOW_H
#define COSF9SLOW_H 1

#include <math.h>

#define   likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

static inline float cosf9slow(float phase) {
  float p = fabsf(phase);
  // p >= 0
  if (likely(p < (1 << 24))) {
    int q = p;
    p -= q;
    // p in 0 .. 1
    p = fabsf(4.0f * p - 2.0f) - 1.0f;
    // p in -1 .. 1
    float s
      = 1.5707963267948965580e+00f * p
      - 6.4596271553942852250e-01f * p * p * p
      + 7.9685048314861006702e-02f * p * p * p * p * p
      - 4.6672571910271187789e-03f * p * p * p * p * p * p * p
      + 1.4859762069630022552e-04f * p * p * p * p * p * p * p * p * p;
    // compiler figures out optimal simd multiplications
    return s;
  } else {
    if (unlikely(isnanf(p) || isinff(p))) {
      // return NaN
      return p - p;
    } else {
      // int could overflow, and it will be integral anyway
      return 1.0f;
    }
  }
}

#endif
