#ifndef COSF7_H
#define COSF7_H 1

extern void cosf7s1(int n, const float * restrict in, float * restrict out);
extern void cosf7s2(int n, const float * restrict in, float * restrict out, float * restrict tmp);

#endif
